package com.canvass.web.server;

import com.canvass.commons.data.Bill;
import com.canvass.commons.data.BillVersion;
import com.canvass.commons.data.DataStore;
import com.canvass.web.client.GreetingService;
import com.canvass.web.service.JsonBill;
import com.canvass.web.shared.FieldVerifier;
import com.google.gson.Gson;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class GreetingServiceImpl extends RemoteServiceServlet implements
		GreetingService {
    Logger logger = Logger.getLogger("GreetingServiceImpl");
	private ArrayList<JsonBill> jsonBills = new ArrayList<JsonBill>();
	
	public String greetServer(String input) throws IllegalArgumentException {
		// Verify that the input is valid. 
		if (!FieldVerifier.isValidName(input)) {
			// If the input is not valid, throw an IllegalArgumentException back to
			// the client.
			throw new IllegalArgumentException(
					"Must have a valid selection");
		}

		String serverInfo = getServletContext().getServerInfo();
		String userAgent = getThreadLocalRequest().getHeader("User-Agent");

		// Escape data from the client to avoid cross-site script vulnerabilities.
		input = escapeHtml(input);
		userAgent = escapeHtml(userAgent);

		return "You have successfully voted for: " + input + ".";
	}

	/**
	 * Escape an html string. Escaping data received from the client helps to
	 * prevent cross-site script vulnerabilities.
	 * 
	 * @param html the html string to escape
	 * @return the escaped string
	 */
	private String escapeHtml(String html) {
		if (html == null) {
			return null;
		}
		return html.replaceAll("&", "&amp;").replaceAll("<", "&lt;")
				.replaceAll(">", "&gt;");
	}

	@Override
	public String getBillList() {
		/**********CSV LOOKUP IS KILLING THE JAVASCRIPT - PUT THIS IN MEMORY***********/
        jsonBills.clear();
		if (jsonBills.size()==0) {
            List<Bill> bills = DataStore.loadCurrentBills();
            for (Bill bill : bills) {
                BillVersion billVersion = bill.getCurrentVersion();
                JsonBill jsonBill = new JsonBill();
                jsonBill.description = billVersion.getDescription();
                jsonBill.name = billVersion.getTitle();
                jsonBill.number = bill.getNumber();
                logger.info(jsonBill.name);
                jsonBills.add(jsonBill);
            }
        }
		Gson gson = new Gson();
		String o=gson.toJson(jsonBills);
        logger.info(o);
		return o;
	}
}
