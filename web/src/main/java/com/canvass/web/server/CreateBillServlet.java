package com.canvass.web.server;

import com.canvass.commons.data.Bill;
import com.canvass.commons.data.BillVersion;
import com.canvass.commons.data.DataStore;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * Created By: jordancote
 * Created On: 11/13/13
 */
public class CreateBillServlet extends HttpServlet {
    Logger logger = Logger.getLogger("CreateBill");

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String billNumber = req.getParameter("bill_number");
        String billTitle = req.getParameter("bill_title");
        String billHtml = req.getParameter("bill_html");

        logger.info(billNumber);
        logger.info(billTitle);
        logger.info(billHtml);

        int billNumberInt;
        try {
            billNumberInt = Integer.parseInt(billNumber);
        } catch (NumberFormatException e) {
            res.getWriter().print("failure");
            return;
        }

        Bill bill = new Bill();
        bill.setNumber(billNumberInt);
        BillVersion billVersion = new BillVersion();
        billVersion.setParentBill(bill);
        bill.setCurrentVersion(billVersion);
        billVersion.setTitle(billTitle);
        billVersion.setDescription(billHtml);
        DataStore.save(bill, billVersion);

        res.getWriter().print("success");
    }
}
