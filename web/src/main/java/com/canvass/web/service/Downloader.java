package com.canvass.web.service;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPConnectionClosedException;
import org.apache.commons.net.ftp.FTPReply;
public class Downloader {
	final FTPClient ftp;
	
	public Downloader(){
		ftp = new FTPClient();
		
	}
	
	public void connect(String server, int port){
		try
        {
            int reply;
            if (port > 0) {
                ftp.connect(server, port);
            } else {
                ftp.connect(server);
            }
            System.out.println("Connected to " + server + " on " + (port>0 ? port : ftp.getDefaultPort()));

            // After connection attempt, you should check the reply code to verify
            // success.
            reply = ftp.getReplyCode();

            if (!FTPReply.isPositiveCompletion(reply))
            {
                ftp.disconnect();
                System.err.println("FTP server refused connection.");
                System.exit(1);
            }
        }
        catch (IOException e)
        {
            if (ftp.isConnected())
            {
                try
                {
                    ftp.disconnect();
                }
                catch (IOException f)
                {
                    // do nothing
                }
            }
            System.err.println("Could not connect to server.");
            e.printStackTrace();
            System.exit(1);
        }
	}
	
	public void downloadBills(String remote, String local, String username, String password){
		boolean error;
	try {	
		__main:
        if (!ftp.login(username, password))
        {
            ftp.logout();
            error = true;
            break __main;
        }

        System.out.println("Remote system is " + ftp.getSystemType());

//        ftp.setFileType(FTP.BINARY_FILE_TYPE);

        // Use passive mode as default because most of us are
        // behind firewalls these days.
        ftp.enterLocalPassiveMode();

//        ftp.setUseEPSVwithIPv4(useEpsvWithIPv4);

        OutputStream output;

        output = new FileOutputStream(local);

        ftp.retrieveFile(remote, output);
        System.out.println(ftp.getReplyString());

        output.close();

        ftp.noop(); // check that control connection is working OK

        ftp.logout();
            
	} catch (FTPConnectionClosedException e)
            {
                error = true;
                System.err.println("Server closed connection.");
                e.printStackTrace();
            }
            catch (IOException e)
            {
                error = true;
                e.printStackTrace();
            }
            finally
            {
                if (ftp.isConnected())
                {
                    try
                    {
                        ftp.disconnect();
                    }
                    catch (IOException f)
                    {
                        // do nothing
                    }
                }
            }
	}

	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String server = "ftp.cga.ct.gov";
		com.canvass.web.service.Downloader dl = new Downloader();
		dl.connect(server, 0);
		dl.downloadBills("pub/data/bill_info.csv", "bill_info.csv", "Anonymous", "");
		
//		com.canvass.web.service.CSVLoader loader = new CSVLoader();
		
	}

}
