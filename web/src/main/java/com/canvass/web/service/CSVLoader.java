package com.canvass.web.service;
import au.com.bytecode.opencsv.CSVReader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

public class CSVLoader {
	CSVReader csv;
	
	public void load(List<JsonBill> jsonBills) {
		
		java.io.File file = new File("bill_info.csv");
		if (!file.exists()) {
			// fetch the file from CT server
			String server = "ftp.cga.ct.gov";
			com.canvass.web.service.Downloader dl = new Downloader();
			dl.connect(server, 0);
			dl.downloadBills("pub/data/bill_info.csv", "bill_info.csv", "Anonymous", "");
		}
		
		FileReader reader;
		try {
			reader = new FileReader("bill_info.csv");
			csv = new CSVReader(reader);
			System.out.println("bill csv opened");
			 String [] nextLine;
			 int count = 1;
			  while ((nextLine = csv.readNext()) != null) {
			      JsonBill next = new JsonBill();
				  // nextLine[] is an array of values from the line
			      next.number = count;
			      next.name = nextLine[5];
			      next.description = nextLine[6];
			      jsonBills.add(next);
			      count++;
			  }
			  System.out.println(String.valueOf(jsonBills.size()));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
