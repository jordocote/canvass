package com.canvass.web.client.page;
import com.canvass.web.client.Constants;
import com.canvass.web.client.EndlessPanel;
import com.canvass.web.client.ui.BillListItem;
import com.google.gwt.json.client.*;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Widget;

import java.util.List;

public class ListBillsPage extends Page {
	private JSONArray JSONBills = new JSONArray();
	private List<HorizontalPanel> billPanels;
	private int nextBillIndex = 0;
	private ListBillsPage self = this;

	
	
	public ListBillsPage(PageManager pageManager) {
        super(Constants.PAGE_NAME_LIST_BILLS, pageManager);
		getBills();


	}
	
	
	private void getBills() {
		manager.getGreetingService().getBillList(
				new AsyncCallback<String>() {
					public void onFailure(Throwable caught) {
						// Show the RPC error message to the user
		
					}

					public void onSuccess(String result) {
                        displayBillList(result);
					}
				});
	}

    private void displayBillList(String result) {
        JSONValue JSONResult =JSONParser.parseStrict(result);
        JSONBills = JSONResult.isArray();
        if (JSONBills == null || JSONBills.size() == 0) {
            //TODO: error message
            return;
        }
        System.out.println(JSONBills.size());


        rootPanel = new EndlessPanel(self.manager) {

            @Override
            public Widget newContent() {
                return nextBillElement();
            }
        };
        //					rootPanel.setStylePrimaryName("");
        for (int i = 0; i<15; i++) {
            BillListItem nextBillListItem = nextBillElement();
            if (nextBillListItem == null) {
                break;
            }
            ((EndlessPanel)rootPanel).getContentPanel().add(nextBillListItem);
        }
        self.show();
    }

	
	private BillListItem nextBillElement() {
		int i = nextBillIndex++;
        if (i >= JSONBills.size()) {
            return null;
        }
		
		final JSONObject JSONBill = JSONBills.get(i).isObject();
		if (JSONBill== null) return null; //todo: graceful
		final JSONString description = JSONBill.get("description").isString();
        final JSONString name = JSONBill.get("name").isString();
		final JSONNumber number = JSONBill.get("number").isNumber();
		//if (number == null || description == null || name == null) {
		//    return null; //todo
		//}


        BillListItem billListItem = new BillListItem(manager, JSONBill)
            .setNumber(number.toString())
            .setName(name.stringValue())
            .setDescription(description.stringValue());


		return billListItem;

	}
}
