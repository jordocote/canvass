package com.canvass.web.client.page;

import com.canvass.web.client.Canvass;
import com.canvass.web.client.GreetingServiceAsync;
import com.google.gwt.animation.client.AnimationScheduler;
import com.google.gwt.animation.client.AnimationScheduler.AnimationCallback;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Panel;

import java.util.HashMap;

public class PageManager extends Timer {
	private HashMap<String, Page> pages = new HashMap<String, Page>();
	private GreetingServiceAsync greetingService;
	private Page currentPage;
	private Panel parentPanel;
	private Canvass canvass;
	
	public PageManager(Panel parentPanel, GreetingServiceAsync greetingService, Canvass canvass) {
		this.parentPanel = parentPanel;
		this.greetingService = greetingService;
		this.canvass = canvass;
	}
	
	public void addPage(Page page) {
		pages.put(page.getName(), page);
	}

	public void removePage(String pageName) {
		pages.remove(pageName);
	}
	
	public Page getPage(String pageName) {
		return pages.get(pageName);
	}
	
	public void switchToPage(String pageName, boolean historyEvent) {
		Page page = pages.get(pageName);
		if (page == null) {
			//error
			System.out.println("cant switch to page: " + pageName);
			return;
		}
		switchToPage(page, historyEvent);
	}
	public void switchToPage(Page page, boolean historyEvent) {
		if (page == null) {
			//error
			System.out.println("cant switch to page (null)");
			return;
		}
		if (!pages.containsKey(page.getName())) {
			addPage(page);
		}
		if (currentPage != null) {
			currentPage.hide();
		}
		loading();
		page.show();
		doneLoading();
		currentPage = page;
		History.newItem(page.getName(), historyEvent);
	}
	protected Panel getParentPanel() {
		return parentPanel;
	}
	protected GreetingServiceAsync getGreetingService() {
		return greetingService;
	}

	public void loading() {
		canvass.loading();
	}
	public void doneLoading() {
		this.schedule(500);
	}
	@Override
	public void run() {
		AnimationScheduler.get().requestAnimationFrame(new AnimationCallback() {
			@Override
			public void execute(double timestamp) {
				canvass.doneLoading();
			}
		});
	}
}
