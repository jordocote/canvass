package com.canvass.web.client.page;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Panel;

public class Page {
	static final protected String SERVER_ERROR = "An error occurred while "
			+ "attempting to contact the server. Please check your network "
			+ "connection and try again.";

	public enum PageStatus {
		DISPLAYED, HIDDEN, CLOSED, INVALID
	};
	protected String name;
	protected Panel rootPanel;
	protected PageManager manager;
	protected PageStatus status;
	
	public Page() {
        this.rootPanel = new FlowPanel();
	}

	public Page(String name, PageManager manager) {
        this();
		this.name = name;
        this.manager = manager;
	}

    public Page(String name, Panel rootPanel, PageManager manager) {
        this(name, manager);
        this.rootPanel = rootPanel;
    }

	public String getName() {
		return name;
	}
	public Panel getRootPanel() {
		return rootPanel;
	}
	public void show() {
		manager.getParentPanel().add(rootPanel);
	}
	public void hide() {
		rootPanel.removeFromParent();
	}
	public void reset() {
		return;
	}
	public void setPageManager(PageManager pageManager) {
		this.manager = pageManager;
		
	}

}
