package com.canvass.web.client.page;

import com.canvass.web.client.Constants;

public class AboutPage extends Page {

	public AboutPage(PageManager manager) {
		super(Constants.PAGE_NAME_ABOUT, manager);
	}
}
