package com.canvass.web.client.page;

import com.canvass.web.client.Constants;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;

/**
 * Created By: jordancote
 * Created On: 11/10/13
 */
public class CreateBillPage extends Page {

    public CreateBillPage(PageManager pageManager) {
        super(Constants.PAGE_NAME_CREATE_BILL, pageManager);


        final VerticalPanel billCreateVerticalPanel = new VerticalPanel();
        billCreateVerticalPanel.setSize("100%", "100%");


        final FormPanel form = new FormPanel();
        form.setAction(Constants.HANDLER_NAME_CREATE_BILL);
        form.setMethod(FormPanel.METHOD_POST);
        form.setWidget(billCreateVerticalPanel);

        // Bill number
        final TextBox billNumberTextBox = new TextBox();
        billNumberTextBox.setName("bill_number");
        billCreateVerticalPanel.add(billNumberTextBox);

        // Bill title
        final TextBox billTitleTextBox = new TextBox();
        billTitleTextBox.setName("bill_title");
        billCreateVerticalPanel.add(billTitleTextBox);

        // Bill html
        final TextBox billHtmlTextBox = new TextBox();
        billHtmlTextBox.setName("bill_html");
        billCreateVerticalPanel.add(billHtmlTextBox);

        // Add a 'submit' button.
        billCreateVerticalPanel.add(new Button("Submit", new ClickHandler() {
            public void onClick(ClickEvent event) {
                form.submit();
            }
        }));

        // Add an event handler to the form.
        form.addSubmitHandler(new FormPanel.SubmitHandler() {
            public void onSubmit(FormPanel.SubmitEvent event) {
                // This event is fired just before the form is submitted. We can take
                // this opportunity to perform validation.
                if (billNumberTextBox.getText().length() == 0) {
                    Window.alert("Must enter a bill number");
                    event.cancel();
                }
                if (billTitleTextBox.getText().length() == 0) {
                    Window.alert("Must enter a bill title");
                    event.cancel();
                }
                if (billHtmlTextBox.getText().length() == 0) {
                    Window.alert("Must enter bill html");
                    event.cancel();
                }
            }
        });
        form.addSubmitCompleteHandler(new FormPanel.SubmitCompleteHandler() {
            public void onSubmitComplete(FormPanel.SubmitCompleteEvent event) {
                Window.alert(event.getResults());
            }
        });

        this.getRootPanel().add(form);

    }
}
