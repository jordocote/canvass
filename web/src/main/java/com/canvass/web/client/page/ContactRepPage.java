package com.canvass.web.client.page;

import com.canvass.web.client.Constants;

public class ContactRepPage extends Page {

	public ContactRepPage() {
		super();
	}

	public ContactRepPage(PageManager manager) {
		super(Constants.PAGE_NAME_CONTACT, manager);
	}
}
