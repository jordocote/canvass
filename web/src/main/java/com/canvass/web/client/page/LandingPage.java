package com.canvass.web.client.page;

import com.canvass.web.client.Constants;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.MenuBar;
import com.google.gwt.user.client.ui.MenuItem;

public class LandingPage extends Page {

	LandingPage self = this;
	private FlowPanel mainContentPanel = new FlowPanel();
	private FlowPanel mainPanel = new FlowPanel();
	//private VerticalPanel lowerPanel = new VerticalPanel();
	private final MenuBar bottomPanelMenuBar = new MenuBar();
	
	
	public LandingPage() {
		super();
	}

	public LandingPage(PageManager manager) {
		//super(manager);
		this.name = Constants.PAGE_NAME_LANDING;
		this.manager = manager;
		
		mainContentPanel.setSize("100%", "100%");
		
		generateMenuStrip();
		mainPanel.setStyleName("landingPageMainPanel",true);
		bottomPanelMenuBar.setStylePrimaryName("landingPageMainMenu");
		mainContentPanel.add(mainPanel);
		mainContentPanel.add(bottomPanelMenuBar);
		
		self.getRootPanel().add(mainContentPanel);
		self.show();
		
	}
	
	
	private void generateMenuStrip() {
		//final HorizontalPanel menuStripPanel = new HorizontalPanel();

		MenuItem listBillsItem = new MenuItem("LIST BILLS", new Command() {
			@Override
			public void execute() {
				manager.switchToPage(new ListBillsPage(manager), false);
			}
		});
		
		MenuItem categoriesItem = new MenuItem("CATEGORIES", new Command() {
			@Override
			public void execute() {
				manager.switchToPage(new CategoriesPage(manager), false);
			}
		});
		
		MenuItem contactRepItem = new MenuItem("CONTACT REPRESENTATIVE", new Command() {
			@Override
			public void execute() {
				manager.switchToPage(new ContactRepPage(manager), false);
			}
		});
		
		
		//bottomPanelMenuBar.setStyleName("LandingPageMenu");
		listBillsItem.setStylePrimaryName("menuPageItemLabel");
		categoriesItem.setStylePrimaryName("menuPageItemLabel");
		contactRepItem.setStylePrimaryName("menuPageItemLabel");
		//aboutItem.setStylePrimaryName("menuPageItemLabel");
		//searchItem.setStylePrimaryName("menuPageItemLabel");


		bottomPanelMenuBar.addItem(listBillsItem);
		bottomPanelMenuBar.addItem(categoriesItem);
		bottomPanelMenuBar.addItem(contactRepItem);
		
		//bottomPanelMenuBar.addItem(searchItem);
		//bottomPanelMenuBar.addItem(titleItem);
		//bottomPanelMenuBar.addItem(aboutItem);
		return;
	}
	
	
}
