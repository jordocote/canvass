package com.canvass.web.client.page;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.json.client.JSONNumber;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.VerticalPanel;

public class VotePage extends Page {
    private JSONObject bill;
    private final FlowPanel mainPanel = new FlowPanel();
    
	public VotePage(PageManager manager, JSONObject JSONBill) {
		this.name = "vote";
		this.manager = manager;
		this.bill = JSONBill;
		
		JSONNumber number = JSONBill.get("number").isNumber();
		JSONString description = JSONBill.get("description").isString();
		
		if (number == null ||
		    description == null || description.stringValue().equals("")) {
		    return; 
		}
		rootPanel.add(generateVotingPanel(number.toString(), description.stringValue()));
	}

	@Override
	public void reset() {
		mainPanel.clear();
		JSONString number = bill.get("number").isString();
		JSONString description = bill.get("description").isString();
		generateVotingPanel(number.stringValue(), description.stringValue());
	}

	/**
	 * 
	 */
	public Panel generateVotingPanel(String number, String description) {
		final Button sendButton = new Button("VOTE");
		final Label errorLabel = new Label();
		final Label questionLabel = new Label(number + " : " + description);
		final StringBuffer selectedVote = new StringBuffer();
		final VerticalPanel voteChoicePanel = new VerticalPanel();
		final RadioButton[] voteChoices = new RadioButton[3];
		voteChoices[0] = new RadioButton("poll", "approve");
		voteChoices[1] = new RadioButton("poll", "disapprove");
		voteChoices[2] = new RadioButton("poll", "dont know");

		for (int i=0; i < 3; i++) {
			RadioButton r = voteChoices[i];
			r.setStyleName("voteChoice");
			voteChoicePanel.add(r);
		}		
		
		
		// We can add style names to widgets
		sendButton.addStyleName("sendButton");

		mainPanel.setStyleName("voteChoicePanel", true);

		// Add the nameField and sendButton to the RootPanel
		// Use RootPanel.get() to get the entire body element
		mainPanel.add(questionLabel);
		mainPanel.add(voteChoicePanel);
		mainPanel.add(sendButton);
		mainPanel.add(errorLabel);
				

		// Create the popup dialog box
		final DialogBox dialogBox = new DialogBox();
		dialogBox.setText("Confirm your selection");
		dialogBox.setAnimationEnabled(true);
		final Label textToServerLabel = new Label();
		final Button yesButton = new Button("Yes");
		final Button noButton = new Button("No");
		// We can set the id of a widget by accessing its Element
		yesButton.getElement().setId("yesButton");
		noButton.getElement().setId("noButton");
		VerticalPanel dialogVPanel = new VerticalPanel();
		dialogVPanel.addStyleName("dialogVPanel");
		dialogVPanel.add(new HTML("<b>Are you sure you want to vote for:</b>"));
		dialogVPanel.add(textToServerLabel);
		dialogVPanel.setHorizontalAlignment(VerticalPanel.ALIGN_RIGHT);
		dialogVPanel.add(yesButton);
		dialogVPanel.add(noButton);
		dialogBox.setWidget(dialogVPanel);

		
		// Add a handler to close the DialogBox
		noButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				dialogBox.hide();
				sendButton.setEnabled(true);
				sendButton.setFocus(true);
			}
		});

		
		// Create a handler for the sendButton
		class MyHandler implements ClickHandler, KeyUpHandler {
			/**
			 * Fired when the user clicks on the sendButton.
			 */
			public void onClick(ClickEvent event) {
				sendVoteToServer();
			}

			/**
			 * Fired when the user types in the nameField.
			 */
			public void onKeyUp(KeyUpEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					sendVoteToServer();
				}
			}

			/**
			 * Send the name from the nameField to the server and wait for a response.
			 */
			private void sendVoteToServer() {
				
				// First, we validate the input.
/*				String textToServer = nameField.getText();
				if (!FieldVerifier.isValidName(textToServer)) {
					errorLabel.setText("Please enter at least four characters");
					return;
				}
*/
				final Label serverResponse = new Label();
				// Then, we send the input to the server.
				sendButton.setEnabled(false);
				textToServerLabel.setText(selectedVote.toString());
				mainPanel.clear();
				mainPanel.add(serverResponse);
				manager.getGreetingService().greetServer(selectedVote.toString(),
						new AsyncCallback<String>() {
							public void onFailure(Throwable caught) {
								// Show the RPC error message to the user
								dialogBox.hide();
								serverResponse.addStyleName("serverResponseLabelError");
								serverResponse.setText(SERVER_ERROR);
				
							}

							public void onSuccess(String result) {
								dialogBox.hide();
								serverResponse
										.removeStyleName("serverResponseLabelError");
								serverResponse.setText(result);

								final Button refreshButton = new Button("find new bills");
								refreshButton.setStyleName("refreshButton");
								
								refreshButton.addClickHandler(new ClickHandler() {					
									@Override
									public void onClick(ClickEvent event) {
											refreshButton.removeFromParent();
											History.back();
									}
								});
								mainPanel.add(refreshButton);
								
							}
						});
			}
		}

		// Add a handler to confirm vote
		yesButton.addClickHandler(new MyHandler());

		sendButton.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				errorLabel.setText("");
				for (int i=0; i<3; i++) {
					RadioButton r = voteChoices[i];
					if (r.getValue()) {
						selectedVote.delete(0, selectedVote.length());
						selectedVote.append(r.getText());
						break;
					}
				}
				if (selectedVote.toString().isEmpty()) {
					//no selection
					errorLabel.setText("Please make a selection before voting");
					return;
				}
				textToServerLabel.setText(selectedVote.toString());
				dialogBox.center();
				noButton.setFocus(true);
			}
		});
		
		return mainPanel;

	}


	
}
