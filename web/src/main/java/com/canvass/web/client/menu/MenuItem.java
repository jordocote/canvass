package com.canvass.web.client.menu;

import com.google.gwt.user.client.ui.Label;

public class MenuItem {
	private final Label itemLabel;
	private MenuManager menuManager;
	
	public MenuItem(String label) {
		itemLabel = new Label(label);
		itemLabel.setStylePrimaryName("menuItem");
	}
	
	public MenuItem(String label, MenuManager menuManager) {
		this(label);
		this.menuManager = menuManager;
		menuManager.addMenuItem(label, this);
	}
	
	protected void setMenuManager(MenuManager menuManager) {
		this.menuManager = menuManager;
	}
	
	public void select() {
	}
	
	public void unselect() {
		
	}
}
