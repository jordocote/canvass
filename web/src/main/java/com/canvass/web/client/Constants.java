package com.canvass.web.client;

/**
 * Created By: jordancote
 * Created On: 11/12/13
 */
public class Constants {
    public static final String PAGE_NAME_ABOUT = "about";
    public static final String PAGE_NAME_LIST_BILLS = "listbills";
    public static final String PAGE_NAME_CREATE_BILL = "createbill";
    public static final String PAGE_NAME_VOTE = "vote";
    public static final String PAGE_NAME_LOGIN = "login";
    public static final String PAGE_NAME_CONTACT = "contact";
    public static final String PAGE_NAME_LANDING = "landing";
    public static final String PAGE_NAME_CATEGORIES = "categories";

    public static final String HANDLER_NAME_CREATE_BILL = "/canvass/createbillsubmit";
}
