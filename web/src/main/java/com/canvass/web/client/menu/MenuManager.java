package com.canvass.web.client.menu;

import java.util.HashMap;

public class MenuManager {
	private HashMap<String, MenuItem> menuItems = new HashMap<String, MenuItem>();

	public MenuManager() {
		
	}
	
	public void addMenuItem(String itemName, MenuItem menuItem) {
		menuItems.put(itemName, menuItem);
	}
	
	public void addMenuItem(String itemName) {
		MenuItem menuItem = new MenuItem(itemName, this);
		addMenuItem(itemName, menuItem);
	}
	
	public void removeMenuItem(String itemName) {
		menuItems.remove(itemName);
	}
}
