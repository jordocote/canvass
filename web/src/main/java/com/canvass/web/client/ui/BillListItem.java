package com.canvass.web.client.ui;

import com.canvass.web.client.page.PageManager;
import com.canvass.web.client.page.VotePage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

/**
 * Created By: jordancote
 * Created On: 11/15/13
 */
public class BillListItem extends Composite {
    interface MyUiBinder extends UiBinder<Widget, BillListItem> {}
    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    @UiField DivElement numberDiv;

    @UiField DivElement nameDiv;

    @UiField DivElement descriptionDiv;

    public BillListItem(final PageManager pageManager, final JSONObject jsonBill) {
        initWidget(uiBinder.createAndBindUi(this));
        this.sinkEvents(Event.ONCLICK);
        this.addHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                pageManager.switchToPage(new VotePage(pageManager, jsonBill), false);
            }
        }, ClickEvent.getType());
    }

    public BillListItem setNumber(String number) {
        numberDiv.setInnerText(number);
        return this;
    }

    public BillListItem setName(String name) {
        nameDiv.setInnerText(name);
        return this;
    }

    public BillListItem setDescription(String desc) {
        descriptionDiv.setInnerText(desc);
        return this;
    }
}
