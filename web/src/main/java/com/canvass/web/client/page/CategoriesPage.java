package com.canvass.web.client.page;

import com.canvass.web.client.Constants;

public class CategoriesPage extends Page {

	public CategoriesPage() {
		super();
	}

	public CategoriesPage(PageManager manager) {
		super(Constants.PAGE_NAME_CATEGORIES, manager);
	}
}
