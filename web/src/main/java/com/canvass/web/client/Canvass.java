/*TODO:
* switch to RequestFactory
*/

package com.canvass.web.client;

import com.canvass.web.client.page.*;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.MenuBar;
import com.google.gwt.user.client.ui.MenuItem;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import com.google.gwt.user.client.ui.Image;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Canvass implements EntryPoint, ValueChangeHandler<String> {
	/**
	 * The message displayed to the user when the server cannot be reached or
	 * returns an error.
	 */
	private static final String SERVER_ERROR = "An error occurred while "
			+ "attempting to contact the server. Please check your network "
			+ "connection and try again.";

	/**
	 * Create a remote service proxy to talk to the server-side Greeting service.
	 */
	private final GreetingServiceAsync greetingService = GWT
			.create(GreetingService.class);
	private final MenuBar menuBar = new MenuBar();
	private final MenuBar logoBar = new MenuBar();
	private FlowPanel menuPanel = new FlowPanel();
	private FlowPanel mainPanel = new FlowPanel();
	private FlowPanel bottomPanel = new FlowPanel();
	private Image loadingImage = new Image("ajax-loader.gif");
	private PageManager pageManager = new PageManager(mainPanel, greetingService, this);
	private DockLayoutPanel dockPanel = new DockLayoutPanel(Unit.PX);
	private boolean locked = false;
	
	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		RootLayoutPanel rootLayoutPanel = RootLayoutPanel.get();
		rootLayoutPanel.add(dockPanel);
		Window.setTitle("Canvass");
		
		generateMenuStrip();

		dockPanel.setStylePrimaryName("dockPanel");
		
		mainPanel.setStylePrimaryName("mainPanel");
		bottomPanel.setStyleName("bottomPanel", true);
		
		dockPanel.addNorth(menuPanel, 165);
		dockPanel.addSouth(bottomPanel, 25);
		dockPanel.add(mainPanel);

		loadingImage.setAltText("loading...");
		loadingImage.setStyleName("loadingImage");
		DOM.getParent(mainPanel.getElement()).appendChild(loadingImage.getElement());
		
		final LandingPage landingPage = new LandingPage(pageManager);
		pageManager.addPage(landingPage);
		pageManager.switchToPage(landingPage, true);
		History.addValueChangeHandler(this);


		doneLoading();
	}
	
	private void generateMenuStrip() {
		//final HorizontalPanel menuStripPanel = new HorizontalPanel();

		MenuItem titleItem = new MenuItem("", new Command() {
			@Override
			public void execute() {
				pageManager.switchToPage(new LandingPage(pageManager), false);
			}
		});
		
		MenuItem listBillsItem = new MenuItem("LIST OF BILLS", new Command() {
			@Override
			public void execute() {
				pageManager.switchToPage(new ListBillsPage(pageManager), false);
			}
		});
		
		MenuItem contactRepItem = new MenuItem("CONTACT", new Command() {
			@Override
			public void execute() {
				pageManager.switchToPage(new ContactRepPage(pageManager), false);
			}
		});
		
		MenuItem aboutItem = new MenuItem("ABOUT", new Command() {
			@Override
			public void execute() {
				pageManager.switchToPage(new AboutPage(pageManager), false);
			}
		});

        MenuItem categoriesItem = new MenuItem("CATEGORIES", new Command() {
            @Override
            public void execute() {
                pageManager.switchToPage(new CategoriesPage(pageManager), false);
            }
        });

		MenuItem createBillItem = new MenuItem("CREATE BILL", new Command() {
			@Override
			public void execute() {
				pageManager.switchToPage(new CreateBillPage(pageManager), false);
			}
		});
		

		titleItem.setStylePrimaryName("titleLabel");
		menuBar.setStylePrimaryName("topPanel");
		listBillsItem.setStylePrimaryName("menuItemLabel");
		contactRepItem.setStylePrimaryName("menuItemLabel");
		aboutItem.setStylePrimaryName("menuItemLabel");
        categoriesItem.setStylePrimaryName("menuItemLabel");
		createBillItem.setStylePrimaryName("menuItemLabel");


		logoBar.addItem(titleItem);
		menuBar.addItem(listBillsItem);
		menuBar.addItem(contactRepItem);
		menuBar.addItem(aboutItem);
        menuBar.addItem(categoriesItem);
		menuBar.addItem(createBillItem);
		menuPanel.add(logoBar);
		menuPanel.add(menuBar);
		return;
	}

	
	public void doneLoading() {

		loadingImage.addStyleName("fadeout");
	}
	public void loading(){

		loadingImage.removeStyleName("fadeout");
	}
	
	
	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		String historyToken = event.getValue(); 
		System.out.println("history value changed: " + historyToken);
		
		String billList = Constants.PAGE_NAME_LIST_BILLS;
		if (historyToken.equals(billList)) {
			if (!locked) {
				loading();
				pageManager.switchToPage(historyToken, false);
				doneLoading();
			}
		} else if (historyToken.equals(Constants.PAGE_NAME_VOTE)) {
			pageManager.getPage(Constants.PAGE_NAME_VOTE).reset();
			pageManager.switchToPage(Constants.PAGE_NAME_VOTE, false);
		} else {
			pageManager.switchToPage(historyToken, false);
		}
		
	}
}
