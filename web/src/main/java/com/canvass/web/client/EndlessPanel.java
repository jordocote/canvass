package com.canvass.web.client;

import com.canvass.web.client.page.PageManager;
import com.google.gwt.event.dom.client.ScrollEvent;
import com.google.gwt.event.dom.client.ScrollHandler;
import com.google.gwt.user.client.ui.*;

public abstract class EndlessPanel extends ScrollPanel implements ScrollHandler {
	private FlowPanel flowPanel = new FlowPanel();
	private PageManager manager;
	private Label a = new Label();
	private int height = 500;
	public EndlessPanel() {
		addScrollHandler(this);
		add(flowPanel);
		flowPanel.add(newContent());
		flowPanel.add(a);
		setSize("100%", "100%");
	}
	
	public EndlessPanel(PageManager manager) {
		this();
		this.manager = manager;
	}
	
	@Override
	public void onScroll(ScrollEvent event) {
		
//		a.setText(Integer.toString( getElement().getScrollHeight()-getElement().getScrollTop() - getElement().getClientHeight() ));
		if ( getElement().getScrollHeight() == getElement().getScrollTop() + getElement().getClientHeight() ) {
			if (manager != null) {
				manager.loading();
			}
			for (int i = 0; i < 10; i++) {
                Widget newWidget = newContent();
                if (newWidget == null) {
                    break;
                }
				flowPanel.add(newWidget);
			}
			if (manager != null) {
				manager.doneLoading();
			}
		}
		
	}
	
	public FlowPanel getContentPanel() {
		return flowPanel;
	}
	
	 public abstract Widget newContent();
}
