

package com.canvass.parse;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.cyberneko.html.parsers.DOMParser;
import org.jcote.domino.DOMSearch;
import org.jcote.domino.checkable.NodeCallback;
import org.jcote.domino.checkable.context.DOMContext;
import org.jcote.domino.checkable.context.DOMContextCriteria;
import org.jcote.domino.checkable.tag.TagMatch;
import org.jcote.domino.exception.DOMContextException;
import org.jcote.domino.exception.DOMSearchException;
import org.jcote.domino.util.Wrapper;
import org.xml.sax.InputSource;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.canvass.commons.data.DataStore;
import com.canvass.commons.data.parse.TextOfBill;


public class TextOfBillParser {
	
	abstract private class NodeClosure extends NodeCallback {
		abstract public void perform(Node node);
	}
	
    //
    // MAIN
    //
   
    /** Main. */
    public static void main(String[] argv) throws Exception {
    	TextOfBillParser tobp = new TextOfBillParser();
    	tobp.run(argv);
    } // main(String[])

    public void run(String[] argv) {
    	int parsedSuccessfully = 0;
    	for (int i=0; i<argv.length; i++) {
    		try{
	    		boolean ok = run(argv[i], new InputSource(argv[i]));
	    		if (ok) {
	    			parsedSuccessfully++;
	    		}
    		}catch (Exception e) {
    			e.printStackTrace();
    		}
    	}
    	
    	double coverage = 100.0 * (parsedSuccessfully / argv.length);
    	System.out.println("ANTLR Coverage: "+coverage+"%");
    }
    
    public boolean run(final String url, InputSource source) throws SAXException, IOException, DOMContextException {
        DOMParser dparser = new DOMParser();
        dparser.parse(source);
        DOMSearch dsearch = new DOMSearch();
        
        final Wrapper<Boolean> dontParse = new Wrapper<Boolean>(false);
        final Wrapper<String> title = new Wrapper<String>(new String());
        final Wrapper<String> idCode = new Wrapper<String>(new String());
        final Wrapper<Integer> lcoNumber = new Wrapper<Integer>(0);
        final Wrapper<String> text = new Wrapper<String>(new String());
        final Wrapper<Boolean> inRemTag = new Wrapper<Boolean>(false);
        
		Pattern p = Pattern.compile(".*([0-9]{4})([A-Za-z]{2})-([0-9]{5})-R([0-9]{2})-([A-Za-z]{2})\\.htm\\Z");
		Matcher m = p.matcher(url);

		if (!m.matches()) {
			System.out.println("ERROR: Could not parse url as TOB: " + url);
			return false;
		}
		
		
		int year = Integer.valueOf(m.group(1));
		String designation = m.group(2).toUpperCase();
		int number = Integer.valueOf(m.group(3));
		int revision = Integer.valueOf(m.group(4));
		
        // Bill Number
        // this.new makes this a closure (allows access to this object's private members)
        TagMatch tmMetaNumber = new TagMatch("META", "name", "Number", this.new NodeClosure() {
			@Override
			public void perform(Node node) {
				String id = node.getAttributes().getNamedItem("content").getNodeValue();
//				System.out.println("Number:"+node.getAttributes().getNamedItem("content").getNodeValue());
			}
		});
        
        // Bill Version Title
        TagMatch tmMetaDesc = new TagMatch("META", "name", "Description", this.new NodeClosure() {
			@Override
			public void perform(Node node) {
				String t = node.getAttributes().getNamedItem("content").getNodeValue();
				title.set(t);
				//System.out.println("Title:"+node.getAttributes().getNamedItem("content").getNodeValue());
			}
		});
        
        // Bill Version ID Code
        TagMatch tmBillCode = new TagMatch("FONT", "face", "Abri Barcode39Na", this.new NodeClosure() {
			@Override
			public void perform(Node node) {
		        if (dontParse.get()) {
		        	return;
		        }
				String id = node.getFirstChild().getNodeValue();
				idCode.set(id);
				//System.out.println("Code:"+node.getFirstChild().getNodeValue());
			}
		});           

        // Bill Version Text Section
        DOMContext contextText = new DOMContext(this.new NodeClosure() {
			@Override
			public void perform(Node node) {
				// append the inner text to our string
				text.set(text.get() + node.getTextContent());
				//System.out.println("TextSection:"+node.getTextContent());
			}
		});
        contextText.addCriteria(new DOMContextCriteria().setTypeAsText());
        contextText.addCriteria(new DOMContextCriteria().setTypeAsTagCriteria("font", "face", "Book Antiqua"));
        contextText.addCriteria(new DOMContextCriteria().setTypeAsTag("p"));

        // text section in red (remove text)
        DOMContext contextText2 = new DOMContext(this.new NodeClosure() {
			@Override
			public void perform(Node node) {
				// append the inner text to our string
				if (!inRemTag.get()) {
					System.out.println("Found red text outside rem tag:"+node.getTextContent());
				}
				text.set(text.get() + node.getTextContent());
				//System.out.println("TextSection:"+node.getTextContent());
			}
		});
        contextText2.addCriteria(new DOMContextCriteria().setTypeAsText());
        contextText2.addCriteria(new DOMContextCriteria().setTypeAsTagCriteria("span", "class", "remove"));
        contextText2.addCriteria(new DOMContextCriteria().setTypeAsTagCriteria("font", "face", "Book Antiqua"));
        contextText2.addCriteria(new DOMContextCriteria().setTypeAsTag("p"));        

        // text section in italics
        DOMContext contextText3 = new DOMContext(this.new NodeClosure() {
			@Override
			public void perform(Node node) {
				text.set(text.get() + node.getTextContent());
				//System.out.println("TextSection:"+node.getTextContent());
			}
		});
        contextText3.addCriteria(new DOMContextCriteria().setTypeAsText());
        contextText3.addCriteria(new DOMContextCriteria().setTypeAsTag("i"));
        contextText3.addCriteria(new DOMContextCriteria().setTypeAsTagCriteria("font", "face", "Book Antiqua"));
        contextText3.addCriteria(new DOMContextCriteria().setTypeAsTag("p"));        

        // text section paragraph
        DOMContext contextText4 = new DOMContext(this.new NodeClosure() {
			@Override
			public void perform(Node node) {
				text.set(text.get() + "\n");
				//System.out.println("TextSection:"+node.getTextContent());
			}
		});
        contextText4.addCriteria(new DOMContextCriteria().setTypeAsTag("p"));
        
		// check for font tag inside insert tag (remove)
		DOMContext contextRemoveText = new DOMContext(new NodeCallback() {
    		@Override
			public void perform(Node node) {
    			String content = node.getTextContent();
    			if (StringUtils.isEmpty(content)) {
    				System.out.println("Found an empty bold tag:"+content);
    				return;
    			}
    			if (content.contains("[")) {
    				if (inRemTag.get()) {
        				System.out.println("Found an embedded rem tag:"+content);
        				return;
    				}
    				//System.out.println("Found a rem tag:"+content);
    				inRemTag.set(true);
    				replaceBeforeAfter(content, "\\[", "<rem>", text);
    			} else if (content.contains("]")) {
    				if (!inRemTag.get()) {
        				System.out.println("Found a stray end rem tag:"+content);
        				return;
    				}
    				//System.out.println("Found a rem tag:"+content);
    				inRemTag.set(false);
    				replaceBeforeAfter(content, "\\]", "</rem>", text);
    			} else {
    				System.out.println("Found a bold tag not [ or ]:"+content);
    			}
			}
		});
		contextRemoveText.addCriteria(new DOMContextCriteria().setTypeAsText());
        contextRemoveText.addCriteria(new DOMContextCriteria().setTypeAsTag("b"));
		contextRemoveText.addCriteria(new DOMContextCriteria().setTypeAsTagCriteria("font", "face", "Book Antiqua"));
		contextRemoveText.addCriteria(new DOMContextCriteria().setTypeAsTag("p"));
        
        
		// check for font tag inside insert tag
		DOMContext contextInsertText = new DOMContext(new NodeCallback() {
    		@Override
			public void perform(Node node) {
				text.set(text.get() + "<add>" + node.getTextContent() + "</add>");
			}
		});
		contextInsertText.addCriteria(new DOMContextCriteria().setTypeAsText());
		contextInsertText.addCriteria(new DOMContextCriteria().setTypeAsTag("u"));  
        contextInsertText.addCriteria(new DOMContextCriteria().setTypeAsTagCriteria("font", "face", "Book Antiqua"));
		contextInsertText.addCriteria(new DOMContextCriteria().setTypeAsTagCriteria("u", "class", "insert"));
		contextInsertText.addCriteria(new DOMContextCriteria().setTypeAsTag("p"));
        
		// check for insert tag inside font tag
		DOMContext contextInsertText2 = new DOMContext(new NodeCallback() {
    		@Override
			public void perform(Node node) {
				text.set(text.get() + "<add>" + node.getTextContent() + "</add>");
			}
		});
		contextInsertText2.addCriteria(new DOMContextCriteria().setTypeAsText());
		contextInsertText2.addCriteria(new DOMContextCriteria().setTypeAsTag("u"));  
        contextInsertText2.addCriteria(new DOMContextCriteria().setTypeAsTagCriteria("u", "class", "insert"));
		contextInsertText2.addCriteria(new DOMContextCriteria().setTypeAsTagCriteria("font", "face", "Book Antiqua"));
		contextInsertText2.addCriteria(new DOMContextCriteria().setTypeAsTag("p"));        
        
		// check for insert tag inside font tag
		DOMContext contextInsertText3 = new DOMContext(new NodeCallback() {
    		@Override
			public void perform(Node node) {
				text.set(text.get() + "<add>" + node.getTextContent() + "</add>");
			}
		});
		contextInsertText3.addCriteria(new DOMContextCriteria().setTypeAsText());
        contextInsertText3.addCriteria(new DOMContextCriteria().setTypeAsTagCriteria("u", "class", "insert"));
		contextInsertText3.addCriteria(new DOMContextCriteria().setTypeAsTag("u"));  
		contextInsertText3.addCriteria(new DOMContextCriteria().setTypeAsTagCriteria("font", "face", "Book Antiqua"));
		contextInsertText3.addCriteria(new DOMContextCriteria().setTypeAsTag("p"));        
        
		
        // Statement of purpose / LCO No.
        DOMContext contextStatement = new DOMContext(this.new NodeClosure() {
			@Override
			public void perform(Node node) {
				if (node.getTextContent().startsWith("Statement of Purpose")) {
					// append the inner text to our string
					text.set(text.get() + node.getTextContent() + "\n");
					//System.out.println("TextSection:"+node.getTextContent());
				} else if (node.getTextContent().startsWith("LCO No. ")) {
					int lcoNum = Integer.valueOf(node.getTextContent().substring(8));
					lcoNumber.set(lcoNum);
				}
			}
		});
        contextStatement.addCriteria(new DOMContextCriteria().setTypeAsTagCriteria("font", "face", "Arial"));
        contextStatement.addCriteria(new DOMContextCriteria().setTypeAsTag("i"));
        contextStatement.addCriteria(new DOMContextCriteria().setTypeAsTag("b"));
        contextStatement.addCriteria(new DOMContextCriteria().setTypeAsTag("p"));
        
        
        // Register Callbacks
        
        try {
            dsearch.addCheckable(tmMetaNumber);
			dsearch.addCheckable(tmMetaDesc);
			dsearch.addCheckable(tmBillCode);
			dsearch.addCheckable(contextText);
			dsearch.addCheckable(contextText2);
			dsearch.addCheckable(contextText3);
			dsearch.addCheckable(contextText4);
			dsearch.addCheckable(contextRemoveText);
			dsearch.addCheckable(contextInsertText);
			dsearch.addCheckable(contextInsertText2);
			dsearch.addCheckable(contextInsertText3);
			dsearch.addCheckable(contextStatement);

			dsearch.execute(dparser.getDocument());
		} catch (DOMSearchException e) {
			e.printStackTrace();
		}
        
        if (dontParse.get()) {
        	return false;
        }

        TextOfBill tob = new TextOfBill();
        tob.setUrl(url);
        tob.setBillDesignation(designation);
        tob.setYear(year);
        tob.setBillNumber(number);
        tob.setRevision(revision);
        tob.setIdCode(idCode.get());
        tob.setText(text.get());
        DataStore.save(tob);
        return true;
    }
    
    //
    // Public static methods
    //

    protected void replaceBeforeAfter(String content, String delimiter, String replacement, Wrapper<String> target) {
		Pattern pattern = Pattern.compile("(.*)" + delimiter + "(.*)");
		Matcher matcher = pattern.matcher(content);
		if (!matcher.matches()) {
			return;
		}
		// before
		if (matcher.groupCount() > 0) {
			String before = matcher.group(1);
			target.set(target.get() + before);
		}

		// replacement
		target.set(target.get() + replacement);

		// after
		if (matcher.groupCount() == 2) {
			String after = matcher.group(2);
			target.set(target.get() + after);
		}
	}

	// for <font face="Book Antiqua"> nodes in text
    // remove tags but leave annotations for removed and added content
    private String getSanitizedText(Node node) {
    	String text = new String();
    	if (!node.getNodeName().equalsIgnoreCase("FONT")) {
    		System.out.println("ERROR: must be font node");
    		return null;
    	}
    	NodeList childNodes = node.getChildNodes();
    	for (int i = 0; i < childNodes.getLength(); i++) {
    		Node n = childNodes.item(i);
    		
    		if (n.getNodeType() == Node.TEXT_NODE) {
    			text = text + n.getTextContent();
    			continue;
    		}
    		
//    		if (!n.getNodeName().equalsIgnoreCase("B")) {
//    			text = text + n.getTextContent();
//    			continue;
//    		}
    	}
    	return text;
    }
    
} // class TextOfBill
