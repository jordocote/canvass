package com.canvass.analyze;


import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RecognitionException;

import com.canvass.commons.data.Bill;
import com.canvass.commons.data.BillVersion;
import com.canvass.commons.data.BillsCollection;
import com.canvass.commons.data.parse.TextOfBill;
import com.canvass.parse.TextSectionLexer;
import com.canvass.parse.TextSectionParser;

public class TextOfBillAnalyzer {

	public TextOfBillAnalyzer() {
		// TODO Auto-generated constructor stub
	}

	public boolean run(TextOfBill tob) {
		Bill bill;
		String designation = tob.getBillDesignation();
		int number = tob.getBillNumber();
		int revision = tob.getRevision();

		// Get or Create Bill object
		if (designation.endsWith("R")) {
			bill = BillsCollection.getOrCreateResolution(number);
		} else if (designation.endsWith("B")) {
			bill = BillsCollection.getOrCreateBill(number);
		} else {
			//System.out.println("ERROR: "+url+" is not one of {HR,HB,SR,SB}.");
			// call it a bill for now
			bill = BillsCollection.getOrCreateBill(number);
		}
		
		// Create BillVersion object
        if (bill.hasVersion(revision)) {
			System.out.println("ERROR: TOB url "+tob.getUrl()+" has already been parsed.");
			return false;
		}
		BillVersion billVersion = bill.addVersion(revision);
		

		// create an antlr token stream from text
		CharStream cs = new ANTLRInputStream(tob.getText());
		TextSectionLexer lexer = new TextSectionLexer(cs);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		tokens.setTokenSource(lexer);
		// create the antlr TOB parser
		TextSectionParser parser = new TextSectionParser(tokens);
		// give it the objects to add things to
		//parser.bill = bill;
		//parser.billVersion = billVersion;

	System.out.println(tob.getUrl());
	
		// PARSE WITH ANTLR
		try {
			parser.text();
		} catch (RecognitionException e) {
			// doesnt match
			//System.out.println("ERROR: Parsing text:");
			//System.out.println("TextSection:"+node.getTextContent());
			System.out.println(e.getMessage());
			return false;
		} catch (IllegalArgumentException e) {
			// doesnt match
			//System.out.println("ERROR: Parsing text:");
			//System.out.println("TextSection:"+node.getTextContent());
			System.out.println(e.getMessage());
			return false;
		}

		//BillSection billSection = parser.billSection;

        /*
        if (bill.get() != null) {
        	System.out.println("--BILL--");
        	System.out.println("number:"+bill.get().getNumber());
        	System.out.println("status:"+bill.get().getStatus());
	        if (bill.get().getSponsors() != null) {
	        	Iterator<Sponsor> iter = bill.get().getSponsors().iterator();
	        	while (iter.hasNext()) {
	        		Sponsor sponsor = iter.next();
	        		System.out.println("sponsor type:"+sponsor.getType());
	        		System.out.println("sponsor name:"+sponsor.getName());
	        	}
	        }
        }
        
        if (billVersion.get() != null) {
        	System.out.println("--BILL VERSION--");
        	System.out.println("bill title:"+billVersion.get().getTitle());
        	System.out.println("lco:"+billVersion.get().getLCONumber());
        	System.out.println("id:"+billVersion.get().getId());
        	System.out.println("proposed text:"+billVersion.get().getProposedText());
        	System.out.println("stpurp:"+billVersion.get().getStatementOfPurpose());
        	System.out.println("revision:"+billVersion.get().getRevisionNumber());
        	System.out.println("file:"+billVersion.get().getFileNumber());
        }
        */
/*
		System.out.println("number:"+sectionMod.getId());
		System.out.println("date:"+sectionMod.getEffectiveDate());
		System.out.println("referred:"+sectionMod.getReferredTo());
		System.out.println("resolved:"+sectionMod.getResolvedBy());
		if (sectionMod.getIntroBy()!=null) {
			System.out.println("intro: ["+sectionMod.getIntroBy().getType().name()+"] "+sectionMod.getIntroBy().getName());
		}
		*/
		return true;
	}
}
