package com.canvass.crawl;
import java.util.HashMap;
import java.util.Set;
import org.apache.nutch.crawl.Crawler;
import org.apache.nutch.api.*;
import org.apache.nutch.api.JobManager.JobType;
import org.apache.nutch.api.impl.*;

public class NutchController {
	public static final int NUTCH_PORT = 14934;
	public static final String NUTCH_CONF_ID = "CANVASS_CT";
	public static final String NUTCH_CRAWL_ID = "CGA_FTP";
	
	private NutchApp app;
	private String jobId = null;
	
	public NutchController() {
		app = new NutchApp();
		
		HashMap<String,String> props = new HashMap<String,String>();
		props.put("http.agent.name", "Canvass");
		props.put("ftp.keep.connection", "true");
		props.put("ftp.username", "anonymous");
		props.put("ftp.password", "anonymous");
		props.put("ftp.timeout", "10000"); // in ms
		props.put("ftp.server.timeout", "50000"); // in ms, should be greater than ftp.timeout
		props.put("ftp.content.limit", "65536"); // in bytes
		props.put("ftp.keep.connection", "true");
		props.put("ftp.follow.talk", "true"); // keep logs
		props.put("db.fetch.interval.default", "2592000"); // in seconds
		props.put("db.fetch.interval.max", "7776000"); // in seconds
		props.put("db.ignore.internal.links", "false");
		props.put("fetcher.threads.per.queue", "3");
		props.put("fetcher.threads.fetch", "5");
		props.put("fetcher.parse", "false"); // auto-parse
		props.put("fetcher.server.delay", "5.0"); // in seconds
		props.put("fetcher.store.content", "true");
		props.put("fetcher.verbose", "false");
		props.put("fetcher.max.exceptions.per.queue", "100");
		
		try {
			NutchApp.confMgr.create(NUTCH_CONF_ID, props, false);
		} catch (Exception e) {
			// TODO: log this
			// config already exists
		}
				
		NutchApp.server = new NutchServer(NUTCH_PORT);
	}
	
	public void start() throws Exception {
		if (jobId != null) {
			// already running
			throw new Exception("Job" + jobId + " already started.");
		}
		
		HashMap<String,Object> args = new HashMap<String,Object>();
		args.put("seed", "ftp://ftp.cga.ct.gov/2012/act/Pa/ ftp://ftp.cga.ct.gov/2012/act/sa/ ftp://ftp.cga.ct.gov/2012/cbs/h/ ftp://ftp.cga.ct.gov/2012/cbs/s/ ftp://ftp.cga.ct.gov/2012/fc/ ftp://ftp.cga.ct.gov/2012/tob/h/ ftp://ftp.cga.ct.gov/2012/tob/s/");
		args.put("seedDir","/tmp/urls");
		args.put("depth", 1);
		jobId = NutchApp.jobMgr.create(NUTCH_CRAWL_ID, JobType.CRAWL, NUTCH_CONF_ID, args);
		
		NutchApp.server.start();
	}
	
	public String getJobStatus() {
		try {
			JobStatus status = NutchApp.jobMgr.get(NUTCH_CRAWL_ID, jobId);
			return status.state.toString();
		} catch (Exception e) {
			// TODO log this
			return e.getMessage();
		}
	}
	
	public boolean isServerRunning() {
		return NutchApp.server.isRunning();
	}
	
	public boolean canStop() {
		try {
			return NutchApp.server.canStop();
		} catch (Exception e) {
			// TODO: log this
			return false;
		}
	}
	
	public boolean stop(boolean force) {
		try {
			return NutchApp.server.stop(force);
		} catch (Exception e) {
			// TODO: log this
			return false;
		}
	}
	
	public Set<String> getConfig() throws Exception {
		return NutchApp.confMgr.list();
	}
	
	public static void main(String[] args) throws Exception {
		NutchController nc = new NutchController();
		nc.start();
	}
}
