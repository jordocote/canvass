/**
 * 
 */
package com.canvass.crawl;

import java.io.IOException;
import java.io.StringReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import com.canvass.parse.TextOfBillParser;

/**
 * @author jordancote
 *
 */
public class CrawlDigest {

	Connection conn = null;
	TextOfBillParser tob = null;
	  
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		CrawlDigest digest = new CrawlDigest();
		digest.run();
	}
	
	public CrawlDigest() {
		tob = new TextOfBillParser();
	}

	public void connect() {
        try {
        	System.out.println("connecting...");
            Class.forName("com.mysql.jdbc.Driver").newInstance();
		    conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1/nutch?useReadAheadInput=true&" +
            "user=root&password=surfing");
//		    conn = DriverManager.getConnection("jdbc:mysql://72.229.114.216/nutch?useReadAheadInput=true&" +
//		                                       "user=canvass&password=skunk56");
		    System.out.println("connected.");
		} catch (SQLException ex) {
		    // handle any errors
		    System.out.println("SQLException: " + ex.getMessage());
		    System.out.println("SQLState: " + ex.getSQLState());
		    System.out.println("VendorError: " + ex.getErrorCode());
		} catch (Exception ex) {
            // handle the error
        	ex.printStackTrace();
        }
	}
	
	public void run() {
		int parsedSuccessfully = 0;
		int total = 0;
		
		if (conn == null) {
			connect();
		}
		
		Statement stmt = null;
		ResultSet rs = null;

		try {
			System.out.println("getting pages from nutch db...");
		    stmt = conn.createStatement();
		    rs = stmt.executeQuery("SELECT baseUrl,content FROM webpage WHERE status = 2 limit 500");

		    while (rs.next()) {
	    		String url = rs.getString("baseUrl");
	    		URI uri = new URI(url);
	    		if (url.endsWith(".htm")) {
	    			String[] path = uri.getPath().split("/", 5);
	    			if (path[1].equals("2012")) {
	    				if (path[2].equalsIgnoreCase("tob")) {
	    	    			System.out.println("parsing : "+url);
	    	    			try {
	    	    				total++;
	    		    			boolean ok = tob.run(url, new InputSource(new StringReader(rs.getString("content"))) );
	    			    		if (ok) {
	    			    			parsedSuccessfully++;
	    			    		}
	    	    			} catch (Exception e) {
	    	    				e.printStackTrace();
	    	    			}
	    				}
	    			}
	    		} else {
	    			System.out.println("not .htm : "+url);
	    		}
		    }
		}
		catch (SQLException ex){
		    // handle any errors
		    System.out.println("SQLException: " + ex.getMessage());
		    System.out.println("SQLState: " + ex.getSQLState());
		    System.out.println("VendorError: " + ex.getErrorCode());
		} catch (URISyntaxException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		finally {
		    // it is a good idea to release
		    // resources in a finally{} block
		    // in reverse-order of their creation
		    // if they are no-longer needed

		    if (rs != null) {
		        try {
		            rs.close();
		        } catch (SQLException sqlEx) { } // ignore

		        rs = null;
		    }

		    if (stmt != null) {
		        try {
		            stmt.close();
		        } catch (SQLException sqlEx) { } // ignore

		        stmt = null;
		    }
		}
		
    	float coverage = 100 * parsedSuccessfully / total;
    	System.out.println("Parser Coverage: "+coverage+"%");
	}
}
