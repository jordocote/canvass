grammar TextSection;

@lexer::header {
//package com.canvass.parse;
}

@parser::header {
//package com.canvass.parse;
import java.util.HashMap;
import java.text.ParseException;

}

@members {
    
  private enum ModType {
  	SUBDIVISION,
  	SUBSECTION,
  }
  
  private int getInt(String s) {
    return Integer.parseInt(s);
  }

//  @Override
//  public void reportError(RecognitionException e) {
//    throw new IllegalArgumentException(e);
//  }
}

text : tob EOF;

tob
 :  genasm NEWLINE+
    ( referred NEWLINE+ )?
    ( introby NEWLINE+ )?
    action NEWLINE+
//    ( 
     ( content NEWLINE+  )* 
//    | ( other NEWLINE ) )
    (amend NEWLINE+)?
    statepurp
    NEWLINE+?
 ;

// ##### Level 1 #####

genasm
 :    GENASM
 ;

referred
 : REFERREDTO phrase 
 ;

introby
 :    INTROBY ( NEWLINE ent=entity )+
 ;

action
 :	ENACTED
 |(RESOLVEDBY
   ('the' SENATE 
   |'this' HOUSE ) ':')
 ;

content
 :  ( billsection
//    | o=other {billVersion.setProposedText($o.text);}
    )
 ;
 
amend
 : AMEND
   (NEWLINE amendSection)+
 ;

amendSection
 : SECTION billSec=INT
   NEWLINE date
   NEWLINE (sectionId|NEWSECTION)
 ;

 statepurp
 : STATEPURP
   NEWLINE o=other
 ;
 
billsection
 :   (SECTION|SEC) sec=INT '.'
     ( addsection 
     | addsubsection
     | addsubdivision
     | modsection
     )
 ;

// ##### Level 2 #####

addsection
 : '(NEW)' 
   effective
   o=other
 ;

addsubsection
 :  SECTION sectionId
    ofstatute
    'is'
    ADD
    SUBSECTION subsecId
    ASFOLLOWS 
    effective ':'
    NEWLINE 
    subsecId 
    subsecBlock[false]
 ;
 
addsubdivision
 :  SECTION sectionId
    ofstatute
    'is'
    ADD
    SUBDIVISION subdivId
    ASFOLLOWS 
    effective ':'
    NEWLINE
    subdivId
    subdivBlock[false]
 ;

modsection
 : SECTION sectionId
   ofstatute
   'is'
   REWRITE 
   effective ':'
   NEWLINE 
   modsectionBlock
;

effective
 :   '(' EFFECTIVE (PASSAGE | date) ')'
 ;

// ##### Level 3 #####

modsectionBlock
 : 
 ( //NEWLINE? textBlock[true]
  NEWLINE? subsecExisting[true]
 | NEWLINE? subsecRemove
 | NEWLINE? subsecAdd
 | NEWLINE? subsecRename
 | NEWLINE? subsecToSubdiv
 )+?
 ;

subsecExisting [boolean withTags]
 : subsecId subsecBlock[$withTags]
 ;

subsecRemove
 : REMOPEN ( subsecId subsecBlock[false] )+ REMCLOSE
 ;

subsecAdd
 : ADDOPEN subsecId subsecBlock[false] ADDCLOSE
 ;

subsecRename
 : REMOPEN subsecId REMCLOSE
   ADDOPEN subsecId ADDCLOSE
   subsecBlock[true] 
 ;

subsecToSubdiv
 : subsecId
   (subdivAdd NEWLINE) *
   ADDOPEN subdivId ADDCLOSE
   subdivBlock[true]
   (NEWLINE subdivAdd) *
 ;


subdivExisting [boolean withTags]
 : subdivId subdivBlock[$withTags]
 ;

subdivRemove
 : REMOPEN ( subdivId subdivBlock[false] )+ REMCLOSE
 ;

subdivAdd
 : ADDOPEN subdivId subdivBlock[false] ADDCLOSE
 ;

subdivRename
 : REMOPEN subdivId REMCLOSE
   ADDOPEN subdivId ADDCLOSE
   subdivBlock[true] 
 ;


sublistExisting [boolean withTags]
 : sublistId sublistBlock[$withTags]
 ;

sublistRemove
 : REMOPEN ( sublistId sublistBlock[false] )+ REMCLOSE
 ;

sublistAdd
 : ADDOPEN sublistId sublistBlock[false] ADDCLOSE
 ;

sublistRename
 : REMOPEN sublistId REMCLOSE
   ADDOPEN sublistId ADDCLOSE
   sublistBlock[true] 
 ;


subsecBlock [boolean withTags]
 : 
 ( NEWLINE? textBlock[$withTags]
 | NEWLINE? subdivExisting[$withTags]
 | {$withTags}? NEWLINE? subdivRemove
 | {$withTags}? NEWLINE? subdivAdd
 | {$withTags}? NEWLINE? subdivRename
 )+?
 ;

subdivBlock [boolean withTags]
 : 
 ( NEWLINE? textBlock[$withTags]
 | NEWLINE? sublistExisting[$withTags]
 | {$withTags}? NEWLINE? sublistRemove
 | {$withTags}? NEWLINE? sublistAdd
 | {$withTags}? NEWLINE? sublistRename
 )+?
 ;

sublistBlock [boolean withTags]
 : textBlock[$withTags]
 ;

subsection returns [String subsec]
 : SUBSECTION '(' sub=alpha ')' 'of' 
   {$subsec = $sub.text;}
 ;

subsections returns [String begin, String end]
 : SUBSECTIONS '(' subBegin=alpha ')' 'to' '(' subEnd=alpha ')' ',' INCLUSIVE ',' 'of'
   {$begin = $subBegin.text; $end = $subEnd.text;}
 ;

subdivision returns [String id]
 : SUBDIVISION '(' div=INT ')' 'of' {$id=$div.text;}
 ;

ofstatute returns [int year]
 : 'of' 'the' (yr=INT SUPPLEMENTTO {$year=getInt($yr.text);})? STATUTES 
 ;
        
sectionId returns [String titleIdPart, String sectionIdPart]
 :   a=secId {$titleIdPart = $a.text;}
     '-' 
     b=secId  {$sectionIdPart = $b.text;} 
 ;

secId
 :  INT alpha?
 ;

subsecId returns [String id]
 : '(' a=LOWER ')' {$id=$a.text;} 
 ;

subdivId returns [int id]
 : '(' a=INT ')' {$id=getInt($a.text);}
 ;

sublistId returns [String id]
 : '(' a=UPPER ')' {$id=$a.text;} 
 ;

date returns [String month, int day, int year]
 :  m=MONTH d=INT ',' y=INT {$month = $m.text; $day = getInt($d.text); $year = getInt($y.text);}
 ;
 
entity 
 : ( committee 
   | rep 
   )
 ;
 
committee returns [String name]
 	:	'(' n=phrase ')' {$name = $n.text;}
 	;
  
rep returns [ String name, int district]
 : ('REP.' | 'SEN.' ) 
   n=phrase ',' dist=INT ('th'|'rd'|'st')? DISTRICT { $name = $n.text; $district = getInt($dist.text);}
 ;

phrase
 :	( LOWER | UPPER | '-' | '"' | ',' | '\'' | ';' )+
	;

addtag returns [String contents]
 : ADDOPEN o=other+? ADDCLOSE {$contents = $o.text;}
 ;

remtag returns [String contents]
 : REMOPEN o=other+? REMCLOSE {$contents = $o.text;}
 ;

alpha
 : ( LOWER | UPPER )+
 ;
 
textBlock [boolean withTags]
 : otherNotStart
   ( other
   | {$withTags}? addtag
   | {$withTags}? remtag
   )*
 ;

other
 :   ~(NEWLINE|ADDOPEN|REMOPEN)+
 ;

otherNotStart
 :   ~(NEWLINE|ADDOPEN|REMOPEN|'('|SECTION|AMEND|STATEPURP)
 ;

ADDOPEN: '<add>';
ADDCLOSE: '</add>';
REMOPEN: '<remove>';
REMCLOSE: '</remove>';
AMEND: 'This act shall take effect as follows and shall amend the following sections:';
ADD: 'amended by adding';
ASFOLLOWS: 'as follows';
PASSAGE: 'from passage';
EFFECTIVE: 'Effective';  
REWRITE: 'repealed and the following is substituted in lieu thereof';
STATEPURP: 'Statement of Purpose:';
STATUTES: 'general statutes';
INCLUSIVE: 'inclusive';
SUPPLEMENTTO: 'supplement to the';
NEWSECTION: 'New section';
MONTH:      ('January'|'February'|'March'|'April'|'May'|'June'|'July'|'August'|'September'|'October'|'November'|'December');
SEC:        ('s' | 'S') 'ec.' ;
SECTION:    ('s' | 'S') 'ec' ('.'|'tion') ;
SUBSECTIONS: ('s' | 'S') 'ubsections' ;
SUBSECTION: ('s' | 'S') 'ubsection' ;
SUBDIVISION: ('s' | 'S') 'ubdivision' ;
DISTRICT: ('D'|'d') 'ist' ('.'|'rict');
GENASM:    'General Assembly';
REFERREDTO: 'Referred to';
INTROBY : 'Introduced by:';
ENACTED:  'Be it enacted by the Senate and House of Representatives in General Assembly convened:';
RESOLVEDBY: 'Resolved by';
SENATE: 'Senate';
HOUSE:  'House';
ACT:    ('A'|'a') 'ct';
INT:        ('0'..'9')+ ;
LOWER: ('a'..'z')+ ;
UPPER: ('A'..'Z')+ ;
NEWLINE:    '\r'? '\n' ;
WS  :   (' '|'\t'|'\u000C')+ -> channel(HIDDEN);
