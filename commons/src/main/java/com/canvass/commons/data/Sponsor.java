package com.canvass.commons.data;

public class Sponsor {
	public static enum Type {
		NONE,
		OTHER,
		COMMITTEE,
		REP,
		SENATOR,
	};
	
	private Type type = Type.NONE;
	private String name = null;
	
	public Sponsor(Type type, String name) {
		this.type = type;
		this.name = name;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}