package com.canvass.commons.data;

import java.util.HashMap;

public class BillsCollection {
	private static HashMap<Integer,Bill> bills = new HashMap<Integer,Bill>();
	private static HashMap<Integer,Bill> resolutions = new HashMap<Integer, Bill>();

	public BillsCollection() {
		
	}
	
	// BILL FUNCTIONS
	public static Bill getBill(int number) {
		return bills.get(number);
	}
	
	public static Bill getOrCreateBill(int number) {
		Bill b = getBill(number);
		if (b == null) {
			b = new Bill(number);
			addBill(number, b);
		}
		return b;
	}
	
	public static void addBill(int number, Bill bill) {
		bills.put(number, bill);
	}
	
	// RESOLUTION FUNCTIONS
	public static Bill getResolution(int number) {
		return resolutions.get(number);
	}
	
	public static Bill getOrCreateResolution(int number) {
		Bill b = getResolution(number);
		if (b == null) {
			b = new Bill(number);
			addResolution(number, b);
		}
		return b;
	}
	
	public static void addResolution(int number, Bill bill) {
		resolutions.put(number, bill);
	}
	
}
