package com.canvass.commons.data;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.ImprovedNamingStrategy;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import com.canvass.commons.data.parse.TextOfBill;

import java.util.List;

public class DataStore {
	public static SessionFactory sessionFactory;
	public static ServiceRegistry serviceRegistry;
	
	static {
	    Configuration configuration = new Configuration();
        configuration.setNamingStrategy(new ImprovedNamingStrategy());
	    configuration.addAnnotatedClass(TextOfBill.class);
        configuration.addAnnotatedClass(Bill.class);
        configuration.addAnnotatedClass(BillVersion.class);
	    configuration.configure();
	    serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();        
	    sessionFactory = configuration.buildSessionFactory(serviceRegistry);
	}
	
	public static void save(Object o) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.saveOrUpdate(o);
		session.getTransaction().commit();
		session.close();
	}

    public static void save(Bill bill, BillVersion billVersion) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        bill.setCurrentVersion(billVersion);
        billVersion.setParentBill(bill);
        session.saveOrUpdate(bill);
        session.saveOrUpdate(billVersion);
        session.getTransaction().commit();
        session.close();
    }

    public static List<Bill> loadCurrentBills() {
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("select bill from Bill bill");
        List<Bill> bills = query.list();
        session.close();
        return bills;
    }
}
