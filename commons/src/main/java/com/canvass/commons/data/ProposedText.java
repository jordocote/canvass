package com.canvass.commons.data;

public class ProposedText {
	/* TEXT
	 * Text of the proposed bill version.
	 */
	private String text = null;

	
	public String getText() {
		return text;
	}


	public void setText(String text) {
		this.text = text;
	}


	public ProposedText() {

	}

}
