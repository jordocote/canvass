package com.canvass.commons.data;

public class InvalidSectionRefException extends Exception {
	public InvalidSectionRefException(String string) {
		super(string + " is not a valid title-section reference");
	}
}
