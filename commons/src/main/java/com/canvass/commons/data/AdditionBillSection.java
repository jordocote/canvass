package com.canvass.commons.data;

public class AdditionBillSection extends BillSection {

	private Section addedSection = null;

	/* CONSTRUCTOR
	 * 
	 */
	public AdditionBillSection() {
		
	}

	public Section getAddedSection() {
		return addedSection;
	}

	public void setAddedSection(Section addedSection) {
		this.addedSection = addedSection;
	}
}
