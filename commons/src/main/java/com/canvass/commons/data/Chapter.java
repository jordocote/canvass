package com.canvass.commons.data;

import java.util.HashMap;
/*
 * Chapter:
 * Contain sections of the statutes grouped in specific subject areas.
 * Example: "Chapter 248 Vehicle Highway Use" (which is in Title 14)
 * 
 *  Article:
 *  (used instead of Chapters in Title 42a Uniform Commercial Code) 
 *  Contain sections of the statutes also grouped in specific subject areas.
 *  Example: "Article 2 Sales"; "Article 4A Funds Transfers"
 */

/*
 * Chapter class will be used for both chapters & articles.
 * The parsers must look for both key words!
 */
public class Chapter {
	private int number;
	private char letter;
	private HashMap<String,Section> sections;
	private boolean isSupplement;  	// chapter version may be part of year's supplement (not in general statutes yet)
}
