package com.canvass.commons.data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class BillSection {
	public enum BillSectionType {
		GENERIC,
		SECTION_ADDITION,
		SECTION_MODIFICATION,
		RESOLUTION,
	}
	
	/* BILL VERSION
	 * The version or draft of a bill that this section belongs to.
	 */
	private BillVersion billVersion = null;
	
	/* EFFECTIVE DATE
	 * When this modification takes effect, if passed.
	 */
	private Date effective = null;
	
	private boolean effectiveOnPassage = false;
	
	/* BILL SECTION NUMBER
	 * The number of the Bill Section.
	 * Different from a Section in the General Statutes.
	 */
	private int number = -1;

	/* SECTION TEXT BLOCKS
	 * The Section texts which are to be added in the General Statutes.
	 */
	private HashMap<String,String> textBlocks = new HashMap<String, String>();
	/* CONSTRUCTOR
	 * 
	 */
	public BillSection() {
		
	}
	
	public Date getEffectiveDate() {
		if (this.effectiveOnPassage) {
			return null;
		}
		return effective;
	}

	public void setEffectiveDate(String date) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("MMMMM d yyyy");
		this.effective = sdf.parse(date);
		this.effectiveOnPassage = false;
	}
	
	public void setEffectiveDate(Date effective) {
		this.effective = effective;
		this.effectiveOnPassage = false;
	}

	public void setEffectiveOnPassage(boolean value) {
		this.effectiveOnPassage = value;
	}
	
	public boolean isEffectiveOnPassage() {
		return this.effectiveOnPassage;
	}
	
	public BillVersion getBillVersion() {
		return billVersion;
	}

	public void setBillVersion(BillVersion billVersion) {
		this.billVersion = billVersion;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	// TEXT BLOCK FUNCTIONS
	public String getTextBlock(String s) {
		return textBlocks.get(s.toLowerCase());
	}
	
	public void addTextBlock(String s, String text) {
		textBlocks.put(s.toLowerCase(), text);
	}
	
public void print() {
	System.out.println("--BillSection--");
    if (this.getClass() == ResolutionBillSection.class) {
    	System.out.println("resolved:"+ ( (ResolutionBillSection)(this) ).getResolvedBy() );
    }
    
    if (this.getClass() == ModificationBillSection.class) {
    	System.out.println("mod section id:"+ ( (ModificationBillSection)(this) ).getModifiedSection().getId() );
    	System.out.println("mod title:"+ ( (ModificationBillSection)(this) ).getModifiedSection().getTitle().getId() );
    }
    
    System.out.println("number:"+this.getNumber());
    
    Date d = getEffectiveDate();
    String date;
    if (d == null) {date="effective date:";} else {date = d.toString();}
    System.out.println("date:"+date);
    
    System.out.println("is effective on passage:"+isEffectiveOnPassage());
    

	System.out.println("text:"+this.textBlocks.toString());

}
	
}
