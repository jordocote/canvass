package com.canvass.commons.data;

import java.util.HashMap;
/* 
 *  Title: 
 *  Contain chapters, articles and sections of the statutes grouped in broad subject areas.
 *  Example: "Title 14 � Motor Vehicles"
 *  
 */

public class Title {
	private String id;
//	private HashMap<String, Chapter> chapters;
	private HashMap<String, Section> sections = new HashMap<String, Section>();
	
	
	public Title(String id) {
		this.id = id;
	}

	public Section getTitle(String id) {
		return sections.get(id);
	}
	
	public Section getSection(String id) {
		return sections.get(id);
	}

	
	public Section getOrCreateSection(String id) {
		Section s = getSection(id);
		if (s == null) {
			try {
				s = new Section(id);
			} catch (InvalidSectionRefException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			addSection(id, s);
		}
		return s;
	}
	
	public void addSection(String id, Section section) {
		sections.put(id, section);
		section.setTitle(this);
	}
	
	public String getId() {
		return id;
	}
}
