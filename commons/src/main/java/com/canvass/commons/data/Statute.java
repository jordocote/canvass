
package com.canvass.commons.data;

import java.util.HashMap;

public class Statute {
	private static HashMap<String, Title> titles = new HashMap<String, Title>();
	
	public Statute() {

	}

	public static Title getTitle(String id) {
		return titles.get(id);
	}
	
	public static Title getOrCreateTitle(String id) {
		Title t = getTitle(id);
		if (t == null) {
			t = new Title(id);
			addTitle(id, t);
		}
		return t;
	}
		
	public static void addTitle(String id, Title title) {
		titles.put(id, title);
	}
}
