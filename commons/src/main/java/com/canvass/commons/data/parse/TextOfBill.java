package com.canvass.commons.data.parse;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.NaturalId;


@Entity
@Table(name="textofbill")
public class TextOfBill {
	@Id
	@GeneratedValue
	private long id;
	
	@NaturalId
	protected String url;
	
	@Temporal(TemporalType.TIME)
	private Date updatedOn;
	
	@Temporal(TemporalType.TIME)
	private Date createdOn;
	
	@Basic
	protected int year;
	
	@Basic
	protected String billDesignation;  // HB SB HR SR
	
	@Basic
	protected int revision;
	
	@Basic
	protected String idCode;
	
	@Basic
	protected String lcoNumber;
	
	@Basic
	protected int billNumber;
	
	@Basic
	@Column(columnDefinition="TEXT")
	protected String text;
	
	public TextOfBill() {
		// TODO Auto-generated constructor stub
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getBillDesignation() {
		return billDesignation;
	}

	public void setBillDesignation(String billDesignation) {
		this.billDesignation = billDesignation;
	}

	public int getRevision() {
		return revision;
	}

	public void setRevision(int revision) {
		this.revision = revision;
	}

	public String getIdCode() {
		return idCode;
	}

	public void setIdCode(String idCode) {
		this.idCode = idCode;
	}

	public String getLcoNumber() {
		return lcoNumber;
	}

	public void setLcoNumber(String lcoNumber) {
		this.lcoNumber = lcoNumber;
	}

	public int getBillNumber() {
		return billNumber;
	}

	public void setBillNumber(int billNumber) {
		this.billNumber = billNumber;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

}
