package com.canvass.commons.data;

public class Subsection {

	private Section section = null;
	
	/* text
	 * Content of subsection
	 */
	private String text = null;
	
	/* id
	 * Identifier ie, the 'a' in 'subsection (a)'
	 */
	private String id = null;
	
	public Subsection() {
		// TODO Auto-generated constructor stub
	}
	
	public Subsection(String id) {
		setId(id);
	}

	public Section getSection() {
		return section;
	}

	public void setSection(Section section) {
		this.section = section;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
