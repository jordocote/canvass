package com.canvass.commons.data;

import java.util.HashMap;


public class ModificationBillSection extends BillSection {
	/* MODIFIED SECTION
	 * The Section in the General Statutes that this SectionMod modifies.
	 */
	private Section modifiedSection = null;
	
	private HashMap<Integer,PartialSubdivision> subdivisionUpdates = new HashMap<Integer,PartialSubdivision>();
	private HashMap<String,PartialSubsection> subsectionUpdates = new HashMap<String,PartialSubsection>();
	private HashMap<String,Subsection> subsectionAdditions = new HashMap<String,Subsection>();
	
	/* CONSTRUCTOR
	 * 
	 */
	public ModificationBillSection() {
		
	}

	public Section getModifiedSection() {
		return modifiedSection;
	}

	public void setModifiedSection(Section section) {
		this.modifiedSection = section;
	}

	// SUBSECTION FUNCTIONS
	public PartialSubsection getSubsectionUpdate(String id) {
		return subsectionUpdates.get(id);
	}
		
	public void setSubsectionUpdate(String id, PartialSubsection subsec) {
		subsectionUpdates.put(id, subsec);
	}

	public Subsection getSubsectionAddition(String id) {
		return subsectionAdditions.get(id);
	}
		
	public void setSubsectionAddition(String id, PartialSubsection subsec) {
		subsectionAdditions.put(id, subsec);
	}
	// SUBDIVISION FUNCTIONS
	public PartialSubdivision getSubdivisionUpdate(int id) {
		return subdivisionUpdates.get(id);
	}
		
	public void setSubdivisionUpdate(int id, PartialSubdivision subdiv) {
		subdivisionUpdates.put(id, subdiv);
	}

}
