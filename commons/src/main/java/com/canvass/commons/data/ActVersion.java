package com.canvass.commons.data;

import java.util.Date;
import java.util.HashMap;

public class ActVersion {
	private String sessionName; // February, Reconvened
	//private int sessionYear;  // 2012   have a getter for this from date
	private String label;  // HB, SB, ...
	private int number;
	private String committee; // JUD, FIN...
	private Date date;  // date of version publication
	private String title;  // "AN ACT PROHIBITING PRICE GOUGING DURING SEVERE WEATHER EVENTS"
	private BillVersion text;  // marked-up text of bill (to date)
	private HashMap<String, String> commiteeActions;  // JUD -> Joint Favorable, ...
}
