package com.canvass.commons.data;

/*
 * RESOLUTION
 * A statement by the General Assembly that is not a law. 
 * Used to approve nominations or labor contacts, 
 *  place constitutional amendments on the ballot, 
 *  or express the legislature’s collective opinion.
 */
public class ResolutionBillSection extends BillSection {

	public enum Entity {
		NONE,
		OTHER,
		HOUSE,
		SENATE,
	};
	
	private Entity resolvedBy = Entity.NONE;
	
	public Entity getResolvedBy() {
		return resolvedBy;
	}
	public void setResolvedBy(Entity resolvedBy) {
		this.resolvedBy = resolvedBy;
	}
}
