package com.canvass.commons.data;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * Section:
 * Contain the text of each statute.
 * Example: "Section 14-219 � Speeding" (which is in Chapter 248, which is in Title 14)
 * 
 * For citation purposes, refer to a specific provision of the General Statutes first by title, then by section. 
 * For example, if you are discussing a law on speeding, you would look at Title 14, Section 219, 
 * which would be cited simply as "Conn. Gen. Stat. Sec. 14-219".
 */

/*
 * Abbreviations used in the Annotations to the General Statutes:
 * Each section of the General Statutes provides source and history information. 
 * If the section has been construed by the courts, it further provides a reference to 
 * published judicial opinions interpreting the section.
 * The Connecticut Reports are identified by the letter "C.", 
 * The Appellate Court Reports are identified by the letters "CA", 
 * The Connecticut Supplement is identified by the letters "CS", and 
 * The Circuit Court Reports by "Conn. Cir. Ct."
 * Please note that prior to Volume 1 of the Connecticut Reports, 
 * judicial decisions were reported by the reporters Kirby, Root and Day. 
 * For this reason, citations using the letters "K", "R" or "D" will be found among the annotations.
 */

public class Section {
	private Title title;
	private String id;  // section number: for 42-460a it would be 460a
//	private boolean isSupplement;  	// section may be part of year's supplement (not in general statutes yet)
//	private int year; // optional
//	private BillVersion text;
//	private String publicActs;  // public acts that modify this section & year of act
	private String history;  // historical notes about specific acts
	private String references;  // "see also" pointers to other texts
	private String annotations;  // explanations and interpretations by courts 

	/* subsections
	 * List of ordered subdivisions (if 
	 */
	private HashMap<String,Subsection> subsections = new HashMap<String,Subsection>();

	/* subdivisions
	 * List of ordered subdivisions (if 
	 */
	private HashMap<Integer,Subdivision> subdivisions = new HashMap<Integer,Subdivision>();
	
	public Section() { }

	public Section(String id, Title title) {
		this.setTitle(title);
		this.setId(id);
	}
	
	public Section(String string) throws InvalidSectionRefException {
		//Pattern p = Pattern.compile("^([0-9]+[a-zA-Z]{0,2})-([0-9]+[a-zA-Z]{0,2})$");
		Pattern p = Pattern.compile("^([0-9]+[a-zA-Z]{0,2})$");
		Matcher m = p.matcher(string.trim());
		if (!m.matches()) {
			throw new InvalidSectionRefException(string);
		}
		//String titleId = m.group(1);
		this.setId(m.group(1));
		// find or create parent Title
		/*Title title = Statute.getTitle(titleId);
		if (title == null) {
			title = new Title(titleId);
			Statute.addTitle(titleId, title);
		}
		// add this section to parent Title
		title.addSection(sectionId, this);*/
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Title getTitle() {
		return title;
	}

	public void setTitle(Title title) {
		this.title = title;
	}
	
	// SUBSECTION FUNCTIONS
	public Subsection getSubsection(String id) {
		return subsections.get(id);
	}
	
	public Subsection getOrCreateSubsection(String id) {
		Subsection b = getSubsection(id);
		if (b == null) {
			b = addSubsection(id);
		}
		return b;
	}
	
	public void addSubsection(String id, Subsection subsection) {
		subsections.put(id, subsection);
	}

	public Subsection addSubsection(String id) {
		Subsection subsec = new Subsection(id);
		addSubsection(id, subsec);
		return subsec;
	}
	
	// SUBDIVISION FUNCTIONS
	public Subdivision getSubdivision(int id) {
		return subdivisions.get(id);
	}
	
	public Subdivision getOrCreateSubdivision(int id) {
		Subdivision b = getSubdivision(id);
		if (b == null) {
			b = addSubdivision(id);
		}
		return b;
	}
	
	public void addSubdivision(int id, Subdivision subdivision) {
		subdivisions.put(id, subdivision);
	}

	public Subdivision addSubdivision(int id) {
		Subdivision subsec = new Subdivision(id);
		addSubdivision(id, subsec);
		return subsec;
	}
}
