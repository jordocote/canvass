package com.canvass.commons.data;

public class Subdivision {

	/*
	 * Identifier ie, '1' in 'subdivision (1)'
	 */
	private int id = -1;
	private String text = null;
	
	public Subdivision() {
		
	}

	public Subdivision(int id) {
		setId(id);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}


}
